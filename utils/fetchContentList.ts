﻿import { VisualQuran } from "../typings";

export const fetchContentList = async () => {
    const res = await fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/visualquran`)
    const data= await res.json()
    const contentList: VisualQuran[] = data
    return contentList
}
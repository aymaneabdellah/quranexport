﻿const API_KEY = process.env.NEXT_PUBLIC_API_KEY
const BASE_URL = './api'

const requests = {
  fetchVisualQurans: `${BASE_URL}/visualquran`,
  fetchVisualQuranById: `${BASE_URL}/visualquran/1`

}

export default requests

﻿import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useRecoilState } from 'recoil'
import {
  currentChapterState,
  currentVerseState,
  isArabicState,
} from '../../atoms/VersesAtom'
//import Center from '../components/Center'
import BackgroundVideo from '../../components/BackgroundVideo'
import useQuranApi from '../../hooks/useQuranApi'
import useVisualQuranInfo from '../../hooks/useVisualQuranInfo'

const Home: NextPage = () => {
  const router = useRouter()
  const { id } = router.query

  const visualquran = useVisualQuranInfo(id)
  const [isArabic, setIsArabic] = useRecoilState(isArabicState)
  const verses = useQuranApi()
  const [currentChapter, setCurrentChapter] =
    useRecoilState(currentChapterState)
  const [currentVerse, setCurrentVerse] = useRecoilState(currentVerseState)


  useEffect(() => {
    setCurrentChapter(visualquran?.recitation?.chapterId)
    console.log(currentChapter)
  }, [currentChapter, visualquran?.id])


  

  return (
    <div className="relative max-h-screen overflow-hidden ">
      <main>
        <div>
          <div >
            <BackgroundVideo
              url={`../${visualquran?.video_url}`}
            ></BackgroundVideo>
          </div>

          <div className="absolute top-10 left-0 h-full w-full flex items-center justify-center overflow-x-scroll scrollbar-thin scrollbar-track-rose-600 ">
            {' '}
            <p
              className={`bg-[#00000054] px-10 py-20 text-center text-[6vw]  md:text-[4vw] lg:text[3.5vw]  text-white  ${
                isArabic
                  ? 'font-uthmani font-black leading-normal  '
                  : ' font-semibold'
              }`}
            >
              {' '}
              {isArabic
                ? verses[currentVerse + visualquran?.recitation?.verseStart - 1]
                    ?.text_uthmani
                : verses[currentVerse + visualquran?.recitation?.verseStart - 1]
                    ?.text}
            </p>
          </div>
        </div>
      </main>
    </div>
  )
}

export default Home

import type { AppProps } from 'next/app'
import { RecoilRoot } from 'recoil'
import RightDrawer from '../components/Navigation/RightDrawer'
import Player from '../components/Player'
import '../styles/globals.css'
function MyApp({ Component, pageProps }: AppProps) {
  return (
    <RecoilRoot>
      {/* <RightDrawer /> */}
      {/*  <Playlist />*/}
      {/*   <Header/>
<Sidebar/>
      {/* Higher Order Component */}
      <Component {...pageProps} />
      <Player />
    </RecoilRoot>
  )
}

export default MyApp

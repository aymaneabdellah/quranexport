﻿import {
  getReciters,
  getReciterById,
} from '../../../mockData/reciters'
import type { NextApiRequest, NextApiResponse } from 'next'
import { Reciter } from '../../../typings'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Reciter>
) {
  const method = req.method
  const id = req.query.id

  const result = await getReciterById(+id)

  /*switch (method) {
    case 'GET':
      //+ changes string into number
      result = await getQuranVideoById(+quranVideoId)
    case 'DELETE':
      break
    case 'POST':
      break
    default:
      res.status(405).end(`Method ${method} Not Allowed`)
  }*/
  res.json(result)
}

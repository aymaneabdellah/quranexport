﻿import {getReciters} from '../../mockData/reciters'
import type { NextApiRequest, NextApiResponse } from 'next'
import { Reciter } from '../../typings'


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Reciter[]>
  ) {
    const result = await getReciters()
    res.json(result)
  }
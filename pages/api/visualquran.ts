﻿import {getVisualQurans} from '../../mockData/visualQurans'
import type { NextApiRequest, NextApiResponse } from 'next'
import { VisualQuran } from '../../typings'


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<VisualQuran[]>
  ) {
    const result = await getVisualQurans()
    res.json(result)
  }
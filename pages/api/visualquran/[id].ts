﻿import {
  getVisualQurans,
  getVisualQuranById,
} from '../../../mockData/visualQurans'
import type { NextApiRequest, NextApiResponse } from 'next'
import { VisualQuran } from '../../../typings'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<VisualQuran>
) {
  const method = req.method
  const id = req.query.id

  const result = await getVisualQuranById(+id)

  /*switch (method) {
    case 'GET':
      //+ changes string into number
      result = await getVisualQuranById(+VisualQuranId)
    case 'DELETE':
      break
    case 'POST':
      break
    default:
      res.status(405).end(`Method ${method} Not Allowed`)
  }*/
  res.json(result)
}

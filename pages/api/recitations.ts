﻿import {getRecitations} from '../../mockData/recitations'
import type { NextApiRequest, NextApiResponse } from 'next'
import { Recitation } from '../../typings'


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Recitation[]>
  ) {
    const result = await getRecitations()
    res.json(result)
  }
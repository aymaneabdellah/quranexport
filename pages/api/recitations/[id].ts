﻿import {
  getRecitations,
  getRecitationById,
} from '../../../mockData/recitations'
import type { NextApiRequest, NextApiResponse } from 'next'
import { Recitation } from '../../../typings'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Recitation>
) {
  const method = req.method
  const id = req.query.id

  const result = await getRecitationById(+id)

  /*switch (method) {
    case 'GET':
      //+ changes string into number
      result = await getQuranVideoById(+quranVideoId)
    case 'DELETE':
      break
    case 'POST':
      break
    default:
      res.status(405).end(`Method ${method} Not Allowed`)
  }*/
  res.json(result)
}

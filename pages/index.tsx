﻿import { GetServerSideProps, NextPage } from 'next'
import Head from 'next/head'
import Banner from '../components/Landing/Banner'
import Row from '../components/Landing/Row'
import useVisualQuran from '../hooks/useVisualQuran'
import { VisualQuran } from '../typings'
import { fetchContentList } from '../utils/fetchContentList'

interface Props {
  contentList: VisualQuran[]
}

const Home : NextPage<Props> = ({ contentList }: Props) => {
  //const contentList = useVisualQuran()
  return (
    <div className="relative h-screen bg-gradient-to-b from-gray-900/10 to-[#010511] lg:h-[140vh]">
      <Head>
        <title>Home - VisualQuran</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="relative md:pl-4 pb-24 lg:space-y-24 lg:pl-16">
        <div className='hidden md:inline'>
        <Banner  />
        </div>

        <section className="ml-10 md:space-y-6">
          <Row title="Trending Now" visualQurans={contentList} />
          <Row title="Recently Listened to" visualQurans={contentList} />
          <Row title="Popular releases" visualQurans={contentList} />
          <Row title="Popular recitors" visualQurans={contentList} />
          <Row title="Recently added" visualQurans={contentList} />
        </section>
      </main>
      {/* Modal */}
    </div>
  )
}
export const getServerSideProps : GetServerSideProps = async (context) => {
  const contentList =  await fetchContentList()
  return {
    props : {
      contentList
    }
  }
}


export default Home

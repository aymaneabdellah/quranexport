﻿//https://api.quran.com/api/v4/quran/translations/57?chapter_number=2
//https://api.quran.com/api/v4/quran/verses/uthmani?chapter_number=2
import React, { useEffect, useState } from 'react'
import { useRecoilState } from 'recoil'
import { Translation } from '../typings'
import {
  VersesState,
  currentTranslationState,
  isArabicState,
  currentChapterState,
} from '../atoms/VersesAtom'
import { currentTrackState } from '../atoms/recitationAtom'

function useQuranApi() {
  const [verses, setVerses] = useRecoilState(VersesState)

  const [isArabic, setIsArabic] = useRecoilState(isArabicState)
  const [currentChapter, setCurrentChapter] =
    useRecoilState(currentChapterState)
  const [currentTranslation, setCurrentTranslation] = useRecoilState(
    currentTranslationState
  )

  const [currentTrack, setCurrentTrack] = useRecoilState(currentTrackState)

  useEffect(() => {
    const fetchQuranVerses = async () => {
      var versesInfo
      if (verses) {
        if (isArabic) {
          versesInfo = await fetch(
            `
          https://api.quran.com/api/v4/quran/verses/uthmani?chapter_number=${currentChapter}`
          ).then((res) => {
            return res.json()
          })
          setVerses(versesInfo.verses)
          console.log(currentChapter)
          console.log(versesInfo.verses)
        } else {
          versesInfo = await fetch(
            `https://api.quran.com/api/v4/quran/translations/${currentTranslation}?chapter_number=${currentChapter}`
          ).then((res) => {
            return res.json()
          })
          setVerses(versesInfo.translations)
          console.log(currentChapter)
          console.log(versesInfo.translations)
        }
      }
    }
    fetchQuranVerses()
  }, [currentTranslation, isArabic, currentChapter])

  return verses as unknown as Translation[]
}

export default useQuranApi

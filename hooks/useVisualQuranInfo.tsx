﻿import { useEffect } from 'react'
import { useRecoilState } from 'recoil'
import { currentTrackState } from '../atoms/recitationAtom'
import { VisualQuran } from '../typings'

function useVisualQuranInfo(id: string | string[] | undefined) {
  const [currentTrack, setCurrentTrack] = useRecoilState(currentTrackState)

  useEffect(() => {
    const fetchVisualQuran = async () => {
      const trackInfo = await fetch(
        //temp fix bug, gives default value 1 if undefined
        `../api/visualquran/${id ? id : 1}`
      ).then((res) => {
        return res.json()
      })
      setCurrentTrack(trackInfo)
      console.log(trackInfo)
    }
    fetchVisualQuran()
  }, [currentTrack?.id, id])

  return currentTrack as unknown as VisualQuran
}

export default useVisualQuranInfo

﻿import { useEffect } from 'react'
import { useRecoilState } from 'recoil'
import { contentListState } from '../atoms/visualQuransAtom'
import { VisualQuran } from '../typings'

function useVisualQuran() {
  const [contentList, setContentList] = useRecoilState(contentListState)

  useEffect(() => {
    const fetchVisualQuran = async () => {
      const contentListInfo = await fetch(`../api/visualquran/`).then((res) => {
        return res.json()
      })
      setContentList(contentListInfo)
      console.log(contentListInfo)
    }
    fetchVisualQuran()
  }, [])

  return contentList as unknown as VisualQuran[]
}

export default useVisualQuran

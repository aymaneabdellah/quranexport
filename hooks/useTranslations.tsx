﻿//https://api.quran.com/api/v4/quran/translations/57?chapter_number=2
//https://api.quran.com/api/v4/quran/verses/uthmani?chapter_number=2
import { useEffect } from 'react'
import { useRecoilState } from 'recoil'
import { translationDataState } from '../atoms/translationsAtom'

function useTranslations() {
  const [translations, setTranslations] = useRecoilState(translationDataState)

  useEffect(() => {
    const fetchTranslations = async () => {
      const versesInfo = await fetch(
        //temp fix bug, gives default value 1 if undefined
        `https://api.quran.com/api/v4/resources/translations`
      ).then((res) => {
        return res.json()
      })
      setTranslations(versesInfo.translations)
      console.log(versesInfo.translations)
    }

    fetchTranslations()
  }, [])

  return translations as unknown as []
}

export default useTranslations

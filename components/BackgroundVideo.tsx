﻿import React from 'react'

interface Props {
  url: string
}

const BackgroundVideo = ({ url }: Props) => {
  return (
    <video key={url} loop autoPlay className="h-screen  w-screen object-fill">
      <source src={url} type="video/mp4" />
      Your browser does not support the video tag.
    </video>
  )
}

export default BackgroundVideo

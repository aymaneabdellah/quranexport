﻿import {
  HeartIcon,
  HomeIcon,
  LibraryIcon,
  PlusCircleIcon,
  RssIcon,
} from '@heroicons/react/outline'
import React from 'react'

function Sidebar() {
  return (
    <div className="fixed left-0 top-0 z-10  h-screen w-20 overflow-y-scroll bg-transparent p-5 text-sm text-gray-500 backdrop-blur-[4px] scrollbar-hide">
      <div className="flex h-full flex-col items-center justify-evenly ">
        <button className="flex items-center space-x-2 hover:text-white ">
          <HomeIcon className="h-10 w-10 " />
        </button>

        <button className="flex items-center space-x-2 hover:text-white">
          <LibraryIcon className="h-10 w-10" />
        </button>

        <button className="flex items-center space-x-2 hover:text-white">
          <PlusCircleIcon className="h-10 w-10" />
        </button>
        <button className="flex items-center space-x-2 hover:text-white">
          <HeartIcon className="h-10 w-10" />
        </button>
        <button className="flex items-center space-x-2 hover:text-white">
          <RssIcon className="h-10 w-10" />
        </button>
      </div>

      <hr className="border-t-[0.1px] border-gray-900" />
    </div>
  )
}

export default Sidebar

﻿import { useState } from 'react'
import { AiFillSetting } from 'react-icons/ai'
import { useRecoilState } from 'recoil'
import { currentTranslationState, isArabicState } from '../../atoms/VersesAtom'
import useTranslations from '../../hooks/useTranslations'

const RightDrawer = () => {
  const [isArabic, setIsArabic] = useRecoilState(isArabicState)

  const options = [
    {
      name: 'Arabic',
    },
    {
      name: 'Translation',
    },
  ]
  const translations = useTranslations()
  const [showSidebar, setShowSidebar] = useState(false)
  const [selected, setSelected] = useState(options[0])
  const [currentTranslation, setCurrentTranslation] = useRecoilState(
    currentTranslationState
  )

  // the value of the search field
  const [name, setName] = useState('')

  // the search result
  const [foundTranslation, setFoundTranslations] = useState(translations)

  const filter = (e) => {
    const keyword = e.target.value

    if (keyword !== '') {
      const results = translations.filter((user) => {
        return user.name.toLowerCase().includes(keyword.toLowerCase())
        // Use the toLowerCase() method to make it case-insensitive
      })
      setFoundTranslations(results)
    } else {
      setFoundTranslations(translations)
      // If the text field is empty, show all users
    }

    setName(keyword)
  }

  return (
    <>
      {showSidebar ? (
        <button
          className="fixed right-10 top-6 z-50 flex cursor-pointer items-center text-4xl text-white"
          onClick={() => setShowSidebar(!showSidebar)}
        >
          x
        </button>
      ) : (
        <AiFillSetting
          size={60}
          onClick={() => setShowSidebar(!showSidebar)}
          className="fixed  right-10 top-6 z-30 flex w-28 cursor-pointer items-center"
        ></AiFillSetting>
      )}

      <div
        className={`fixed top-0 right-0 z-40  h-full max-w-lg md:w-80 w-[15rem]  overflow-y-scroll bg-[#0202025f] p-10 pl-2  duration-300 ease-in-out ${
          showSidebar ? 'translate-x-0 ' : 'translate-x-full'
        }`}
      >
        <div className="mt-20">
          <button
            onClick={() => {
              setIsArabic(true)
            }}
            className={` mt-5 w-full p-5 ${
              isArabic ? 'bg-gray-900 text-white' : 'bg-white text-black  '
            }`}
          >
            Arabic
          </button>

          <button
            onClick={() => {
              setIsArabic(false)
            }}
            className={` mt-5 w-full p-5 ${
              !isArabic ? 'bg-gray-900 text-white' : 'bg-white text-black '
            }`}
          >
            Translation
          </button>
          <div className={`${isArabic ? 'hidden' : ''}`}>
            <input
              type="search"
              value={name}
              onChange={filter}
              className="input mt-5 w-full p-5 text-black"
              placeholder="Search Translation"
            />
            <div className=" ">
              {foundTranslation?.map((translation) => (
                <li
                  className={`m-5 p-5 ${
                    translation.id == currentTranslation
                      ? 'bg-gray-900'
                      : 'bg-gray-800'
                  } cursor-pointer list-none hover:bg-gray-700`}
                  onClick={() => {
                    setCurrentTranslation(translation.id)
                  }}
                  key={translation.id}
                >
                  {translation.name}
                </li>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default RightDrawer

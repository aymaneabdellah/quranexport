﻿import { BellIcon, SearchIcon } from '@heroicons/react/solid'
import Link from 'next/link'
import { useEffect, useState } from 'react'

function Header() {
  const [isScrolled, setIsScrolled] = useState(false)

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 0) {
        setIsScrolled(true)
      } else {
        setIsScrolled(false)
      }
    }
    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  })

  return (
    <header
      className={`${isScrolled && 'backdrop-blur-[4px]'} z-10  flex shadow-xl`}
    >
      <div className=" ml-10 flex justify-between space-x-2 md:space-x-10  ">
        <img
          src="https://fontmeme.com/permalink/220509/5b8f6c55b2db30b56e450d4cdab3228f.png"
          width={150}
          height={150}
          className="cursor-pointer object-contain"
          alt=""
        />
      </div>

      <div className="relative text-gray-600 focus-within:text-gray-400 ">
        <span className="absolute inset-y-0 left-0 flex items-center pl-2">
          <button
            type="submit"
            className="focus:shadow-outline p-1 focus:outline-none"
          >
            <SearchIcon className="hidden h-6 w-6 pl-2 sm:inline " />
          </button>
        </span>
        <input
          type="search"
          name="q"
          className="rounded-md bg-[#000000a3] py-2 pl-10  text-sm  text-white focus:bg-white focus:text-gray-900 focus:outline-none"
          placeholder="chapters, reciters..."
        />
      </div>

      <div className="flex cursor-pointer justify-between space-x-4 text-sm font-light">
        <p>Upload</p>
        <BellIcon className="h-6 w-6" />
        <Link href="/account">
          <img
            src="https://rb.gy/g1pwyx"
            alt=""
            className="cursor-pointer rounded"
          />
        </Link>
      </div>
    </header>
  )
}

export default Header

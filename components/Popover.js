﻿import { Popover, Transition } from '@headlessui/react'
import { ViewListIcon } from '@heroicons/react/solid'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import ReactDOM from 'react-dom'
import { usePopper } from 'react-popper'
import useVisualQuran from '../hooks/useVisualQuran'

export default function Example() {
  const contentList = useVisualQuran()
  let [referenceElement, setReferenceElement] = useState()
  let [popperElement, setPopperElement] = useState()
  let { styles, attributes } = usePopper(referenceElement, popperElement)
  return (
    <div className="">
      <Popover className="relative" ref={setReferenceElement}>
        {({ open }) => (
          <>
            <Popover.Button
              className={`
                  ${open ? '' : 'text-opacity-90'}
                `}
            >
              <ViewListIcon
                className={`${open ? '' : 'text-opacity-70'}
                  h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125`}
                aria-hidden="true"
              />
            </Popover.Button>
            <Transition
              enter="transition ease-out duration-200"
              enterFrom="opacity-0 translate-y-1"
              enterTo="opacity-100 translate-y-0"
              leave="transition ease-in duration-150"
              leaveFrom="opacity-100 translate-y-0"
              leaveTo="opacity-0 translate-y-1"
            >
              {typeof window !== 'undefined'
                ? ReactDOM.createPortal(
                    <Popover.Panel
                      ref={setPopperElement}
                      style={styles.popper}
                      {...attributes.popper}
                      className="absolute  z-50 mt-3 w-screen max-w-sm -translate-x-1/2 transform px-4 sm:px-0 lg:max-w-3xl"
                    >
                      <div className=" rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 h-96 overflow-y-scroll scrollbar-hide mr-5">
                        <h1 className="text-center text-5xl">Playlist</h1>
                        {contentList.map((content) => (
                          <Link href={`/quran/${content?.id}`}>
                            <div
                              className={` relative m-5 grid cursor-pointer grid-cols-2 rounded-lg bg-cover py-4 px-5 text-white bg-[#07060663] hover:bg-transparent transition-all duration-200 w-full`}
                            >
                              <Image
                               className='-z-10'
                                src={`${content?.img_url}`}
                                layout="fill"
                                objectFit="cover"
                              ></Image>
                              <div className="flex items-center space-x-4 ">
                                <p>{1}</p>
                                <div className="relative h-10 w-10">
                                  <Image
                                  className='z-50'
                                    src={`${content?.recitation?.reciter?.profile_img_url}`}
                                    layout="fill"
                                    objectFit="cover"
                                  ></Image>
                                </div>

                                <div>
                                  <p className="w-36 truncate lg:w-64">
                                    Surah {content?.recitation?.chapter}
                                  </p>
                                  <p className="w-40">
                                    {content?.recitation?.reciter.name}
                                  </p>
                                </div>
                              </div>

                              <div className="ml-auto flex items-center justify-between md:ml-0">
                                <p className="hidden w-40 md:inline"></p>
                                <p className="">
                                  {content?.recitation?.duration}
                                </p>
                              </div>
                            </div>
                          </Link>
                        ))}
                      </div>
                    </Popover.Panel>,
                    document.querySelector('body')
                  )
                : ''}
            </Transition>
          </>
        )}
      </Popover>
    </div>
  )
}

﻿import { useState } from 'react'

const Banner = () => {
  const [sound, setSound] = useState(true)

  const handleVolume = () => {
    if (sound) {
      setSound(false)
    } else {
      setSound(true)
    }
    console.log(sound)
  }

  return (
    <div className="flex flex-col space-y-2 py-16 md:space-y-4 lg:h-[65vh] lg:justify-end lg:pb-12">
      <div className="absolute top-0 left-0 -z-10 h-screen w-screen">
        {/*   <Image
          src={`${baseUrl}${movie?.backdrop_path || movie?.poster_path}`}
          layout="fill"
          objectFit="cover"
  ></Image>*/}
        <video loop autoPlay className="w-screen" muted={sound}>
          <source src="./assets/videos/Clouds.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </div>
      <h1 className="text-2xl md:text-4xl lg:text-7xl">
        {/*movie?.title || movie?.name || movie?.original_name*/ ''}
      </h1>
      <div className="ml-10">
        {/* <p className="max-w-xs text-xs md:max-w-lg md:text-lg lg:max-w-2xl">
          {/*movie?.overview 'Islam Sobhi | Surah Al Baqarah'</p> 
        
        <div className="flex space-x-3">
         
        <Link href='/quran'>
          <button  className="bannerButton bg-white text-black">
            <PlayIcon className="h-4 w-4 text-black md:h-7 md:w-7" />
            Play
          </button>
          </Link>
          <button className="bannerButton bg-[gray]/70">
            <FlagIcon className="h-5 w-5 md:h-8 md:w-8" />
            Visit Channel
          </button>
          <div onClick={handleVolume}>
            {!sound ? (
              <VolumeUpIcon className="h-10 w-10 cursor-pointer md:h-12 md:w-12 " />
            ) : (
              <VolumeOffIcon className="h-10 w-10 cursor-pointer md:h-12 md:w-12 "></VolumeOffIcon>
            )}
            
          </div>
        </div>*/}
      </div>
    </div>
  )
}

export default Banner

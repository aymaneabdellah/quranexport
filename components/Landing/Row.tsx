﻿import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/outline'
import { useRef, useState } from 'react'
import { VisualQuran } from '../../typings'
import Thumbnail from './Thumbnail'

interface Props {
  title: string
  visualQurans: VisualQuran[] | []
}

function Row({ title, visualQurans }: Props) {
  const rowRef = useRef<HTMLDivElement>(null)
  const [isMoved, setIsMoved] = useState(false)

  const handleClick = (direction: string) => {
    setIsMoved(true)
    if (rowRef.current) {
      const { scrollLeft, clientWidth } = rowRef.current

      const scrollTo =
        direction === 'left'
          ? scrollLeft - clientWidth
          : scrollLeft + clientWidth
      rowRef.current.scrollTo({ left: scrollTo, behavior: 'smooth' })
    }
  }

  return (
    <div className=" space-y-0.5 md:space-y-2">
      <h2 className="w-56 cursor-pointer text-sm font-semibold text-[#e5e5e5] transition duration-200 hover:text-white md:text-2xl">
        {title}
      </h2>
      <div className="group relative md:-ml-2">
        <ChevronLeftIcon
          className={`absolute top-0 bottom-20 left-2 z-50 m-auto h-9 w-9 cursor-pointer opacity-0 transition hover:scale-125 group-hover:opacity-100 ${
            !isMoved && 'hidden'
          }`}
          onClick={() => handleClick('left')}
        />
        <div
          className="flex  space-x-0.5 overflow-x-hidden  overflow-y-visible scrollbar-hide md:space-x-4 md:p-6  " 
          ref={rowRef}
        >
          {visualQurans?.map((visualQuran) => (
            <Thumbnail key={visualQuran.id} visualQuran={visualQuran} />
          ))}
        </div>
        <ChevronRightIcon
          className="absolute top-0 bottom-20 right-2 z-50 m-auto h-9 w-9 cursor-pointer opacity-0 transition hover:scale-125 group-hover:opacity-100"
          onClick={() => handleClick('right')}
        />
      </div>
    </div>
  )
}

export default Row

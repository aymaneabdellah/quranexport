import Image from 'next/image'
import Link from 'next/link'
import { VisualQuran } from '../../typings'

interface Props {
  visualQuran: VisualQuran
}

function Thumbnail({ visualQuran }: Props) {
  return (
    <div
      className={` cursor-pointer transition duration-500 ease-out hover:z-20 md:hover:scale-105 `}
    >
      <Link href={`/quran/${visualQuran.id}`}>
        <div className="relative h-32 w-56  md:h-56 md:w-96 bg-gray-600"   >
          <Image 
            src={visualQuran?.img_url}
            layout="fill"
            objectFit="cover"
          ></Image>
        </div>
      </Link>
      <div className="flex flex-col gap-1 ">
        <div className="font-semibold text-gray-200 ">
          {' '}
          {visualQuran.title}{' '}
        </div>
        <div className="-mt-1 text-sm text-gray-400 ">
          {' '}
          {visualQuran.views} Views{' '}
        </div>

        <div className="flex flex-row flex-wrap gap-2">
          <a href="#" className="tag">
            {' '}
            {visualQuran.recitation?.chapter}{' '}
          </a>
          <a href="#" className="tag">
            {' '}
            {visualQuran.recitation?.reciter?.name}{' '}
          </a>
          <a href="#" className="tag">
            {' '}
            subtitle
          </a>
          <a href="#" className="tag">
            {' '}
            {visualQuran.recitation?.riwaya}{' '}
          </a>
        </div>
      </div>
    </div>
  )
}

export default Thumbnail

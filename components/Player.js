﻿import {
  DesktopComputerIcon,
  PauseIcon,
  PlayIcon,
  VolumeOffIcon,
  VolumeUpIcon,
} from '@heroicons/react/solid'
import Link from 'next/link'
import React, { useRef, useState } from 'react'
import { BiShuffle } from 'react-icons/bi'
import { BsFillSkipEndFill, BsFillSkipStartFill } from 'react-icons/bs'
import { IoRepeatOutline } from 'react-icons/io5'
import ReactPlayer from 'react-player'
import { useRecoilState } from 'recoil'
import { currentTrackState } from '../atoms/recitationAtom'
import { currentVerseState } from '../atoms/VersesAtom'
import { millisToMinutesAndSeconds } from '../lib/time'
import Popover from './Popover'

function Player() {
  const [currentTrack, setCurrentTrack] = useRecoilState(currentTrackState)
  const [currentVerse, setCurrentVerses] = useRecoilState(currentVerseState)
  const [volume, setVolume] = useState(0.3)
  const [played, setPlayed] = useState(0)
  const [duration, setDuration] = useState(0)
  const [loop, setLoop] = useState(false)
  const [seeking, setSeeking] = useState(false)

  const [isPlaying, setIsPlaying] = useState(true)
  const [isMuted, setIsMuted] = useState(false)

  const handlePlayPause = () => {
    setIsPlaying(!isPlaying)
  }

  const handleVolumeChange = (e) => {
    setVolume(parseFloat(e.target.value))
  }

  const handleSeekChange = (e) => {
    setPlayed(parseFloat(e.target.value))
  }
  const handleSeekMouseDown = (e) => {
    setSeeking(true)
  }
  const handleSeekMouseUp = (e) => {
    setSeeking(false)
    audioRef?.current.seekTo(parseFloat(e.target.value))
  }

  const handleToggleLoop = () => {
    setLoop(!loop)
  }

  const handleDuration = (duration) => {
    setDuration(duration)
  }
  const handleProgress = (state) => {
    // console.log(currentTrack?.recitation?.sync[currentVerse]!)

    if (!seeking) {
      setPlayed(state.played)
    }

    var currIndex = currentTrack?.recitation?.timeStamps?.findIndex(
      (index) => index >= audioRef?.current?.getCurrentTime()
    )


    if (currIndex != currentVerse) {
      setCurrentVerses(currIndex)
    }
  } 

  const handleMute = () => {
    if (isMuted) {
      setIsMuted(!isMuted)
      setVolume(parseFloat(0.5))
    } else {
      setIsMuted(!isMuted)
      setVolume(parseFloat(0))
    }
  }

  const audioRef = useRef()

  return (
    <>
      {/* Free Users */}
      <div className={`${currentTrack ? '' : 'hidden'}`}>
        <div className="hidden">
          <ReactPlayer
            ref={audioRef}
            url={currentTrack?.recitation?.url}
            playing={isPlaying}
            muted={isMuted}
            controls={true}
            volume={volume}
            loop={loop}
            onProgress={handleProgress}
            onDuration={handleDuration}
          />
        </div>

        <div className="fixed bottom-0 left-0 z-50 flex h-fit w-full items-center  justify-between  space-x-20 overflow-x-scroll border-2 py-2.5 px-2 text-white  backdrop-blur-[4px] scrollbar-hide md:space-x-0 md:overflow-x-hidden">
          <div className="flex items-center space-x-2">
            <img
              className="hidden h-10 w-10 md:inline"
              src={currentTrack?.recitation?.reciter?.profile_img_url}
              alt=""
            ></img>
            <div className='hidden md:inline'>
              <h4 className="hidden md:block max-w-[150px] truncate text-sm text-white md:max-w-[250px]">{`Surah ${currentTrack?.recitation?.chapter}`}</h4>
              <h5 className="hidden md:inline text-xs text-[rgb(179,179,179)]">
                {currentTrack?.recitation?.reciter?.name}
              </h5>
            </div>
          </div>
          <div className="inset-x-auto flex w-full flex-col items-center space-y-2 md:absolute">
            <div className="flex items-center space-x-4 text-xl text-[#b3b3b3]">
              <BiShuffle className="playerIcon h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125" />
              <Link href={`/quran/${currentTrack?.id - 1}`}>
                <BsFillSkipStartFill className="playerIcon h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125" />
              </Link>
              {isPlaying ? (
                <PauseIcon
                  onClick={handlePlayPause}
                  className="h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125"
                />
              ) : (
                <PlayIcon
                  onClick={handlePlayPause}
                  className="h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125"
                />
              )}
              <Link href={`/quran/${currentTrack?.id + 1}`}>
                <BsFillSkipEndFill className="playerIcon h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125" />
              </Link>
              <IoRepeatOutline
                onClick={handleToggleLoop}
                className={`playerIcon cursor-pointer  transition duration-100 ease-out hover:scale-125 ${
                  loop ? 'text-white' : ''
                }`}
              />
            </div>
            <div className="flex items-center space-x-2.5 text-xs text-[#CECECE] ">
              <h4 className="-mt-0.5">
                {millisToMinutesAndSeconds(duration * played)}
              </h4>

              <input
                type="range"
                min={0}
                max={0.999999}
                step="any"
                className="h-1 w-72 cursor-pointer rounded-xl bg-[#383838] lg:w-[450px]"
                value={played}
                onChange={handleSeekChange}
                onMouseUp={handleSeekMouseUp}
                onMouseDown={handleSeekMouseDown}
              />

              <h4 className="-mt-0.5">{millisToMinutesAndSeconds(duration)}</h4>
            </div>
          </div>

          <div className="flex items-center md:space-x-3 text-[#b3b3b3]">
            <Link href={`/quran/${currentTrack?.id}`}>
              <DesktopComputerIcon className="h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125" />
            </Link>
            <Popover></Popover>
            <div className="flex items-center space-x-3">
              {!isMuted ? (
                <VolumeUpIcon
                  onClick={handleMute}
                  className="h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125"
                />
              ) : (
                <VolumeOffIcon
                  onClick={handleMute}
                  className="h-5 w-5 transform cursor-pointer transition duration-100 ease-out hover:scale-125"
                />
              )}

              <input
                className="z-50 h-1 w-[88px] cursor-pointer rounded-xl bg-[#383838]"
                onChange={handleVolumeChange}
                type="range"
                value={volume}
                min={0}
                max={1}
                step="any"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Player

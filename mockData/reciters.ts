﻿import { Reciter } from '../typings'

export const getReciters = () => {
  return reciterData
}

export const getReciterById = (id: number) => {
  return reciterData.find((reciter) => reciter.id === id) as Reciter
}

export const reciterData = [
  {
    id: 1,
    name: 'Ahmed Khedr',
    arabic_name: 'أحمد خضر',
    dataOfBirth: '17/09/1997',
    profile_img_url:
      'https://scontent-bru2-1.xx.fbcdn.net/v/t39.30808-6/278327161_123529890286022_3981176354021534168_n.jpg?_nc_cat=1&ccb=1-6&_nc_sid=09cbfe&_nc_ohc=k8ww6HcCvlAAX-U5Ztt&_nc_ht=scontent-bru2-1.xx&oh=00_AT_VYw70sGkc3wQuJpZ77pGrJfxyftzoIKfM5_ZMQ_VpUg&oe=6280A5DE',
    style: '',
    isAlive: true,
    isQari: true,
    biography: 'lorem',
  },
]

﻿import { Recitation } from '../typings'

export const getRecitations = () => {
  return recitationData
}

export const getRecitationById = (id: number) => {
  return recitationData.find((recitation) => recitation.id === id) as Recitation
}

export const recitationData = [
  {
    id: 1,
    url: 'string',
    duration: '00:15:00',
    format: '.mp3',
    chapter: 'Baqarah',
    verseStart: 1,
    verseEnd: 7,
    riwaya: 'Warsh',
    reciterId: 1,
    chapterId: 2,
    reciter: {
      id: 1,
      name: 'Ahmed Khedr',
      arabic_name: 'أحمد خضر',
      dataOfBirth: '17/09/1997',
      profile_img_url:
        'https://scontent-bru2-1.xx.fbcdn.net/v/t39.30808-6/278327161_123529890286022_3981176354021534168_n.jpg?_nc_cat=1&ccb=1-6&_nc_sid=09cbfe&_nc_ohc=k8ww6HcCvlAAX-U5Ztt&_nc_ht=scontent-bru2-1.xx&oh=00_AT_VYw70sGkc3wQuJpZ77pGrJfxyftzoIKfM5_ZMQ_VpUg&oe=6280A5DE',
      style: '',
      isAlive: true,
      isQari: true,
      biography: 'lorem',
    },
    timeStamps: [
      19.09726392995213, 28.297263929952134, 35.609263929952135,
      60.377263929952136, 70.84926392995213, 79.51859726328547,
      102.81726392995213, 108.53726392995213, 118.28926392995214,
      128.73726392995212, 136.43859726328546, 153.12926392995212,
      158.6572639299521, 162.3879305966188, 170.76926392995213,
      179.08926392995212, 185.83859726328546, 193.56926392995211,
      201.7772639299521, 205.41726392995213, 220.5383936691018,
      226.41839366910182, 242.15706033576845, 257.9970603357685,
      275.5090603357685, 284.5570603357685, 298.63706033576847,
      321.7970603357685,
    ],
  },
]

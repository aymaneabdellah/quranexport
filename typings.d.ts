﻿export interface VisualQuran {
  id: number
  title: string
  img_url: string
  video_url: string
  popularity: number
  views: number
  likes: number
  created_by: string
  recitation?: Recitation
}


export interface Settings {
  id: number
  arabic:boolean
  translation:boolean
  font: string
  font_color:string
  font_size: string
  arabic_text: number
  translation_text: number
}

export interface Reciter {
  id: number
  name: string
  arabic_name?: string
  dataOfBirth?: date
  profile_img_url?: string
  style?: string
  isAlive?: boolean
  isQari?: boolean
  biography?: string
}

export interface Recitation {
  id: int
  url: string
  duration: string
  format: string
  chapter: string
  verseStart: int
  verseEnd: int
  riwaya?: string
  reciterId?: int
  reciter?: Reciter
  timeStamps: number[]
  chapterId: int
}

export interface Sync {
  id: number
  user: number
  recitation: number
  timestamps: number[]
}

export interface Translation {
  resource_id: number,
  text: string,
  text_uthmani
}

export interface Uthmani {
  resource_id: number,
  verse_key: string,
  text_uthmani: string
}

export interface Uthmani_Simple {
  resource_id: number,
  verse_key: string,
  text_uthmani_simple: string
}


export interface Uthmani_Tajweed {
  resource_id: number,
  verse_key: string,
  text_uthmani_tajweed: string
}

export interface Indopak {
  resource_id: number,
  verse_key: string,
  text_uthmani_indopak: string
}

export interface Imlaei {
  resource_id: number,
  verse_key: string,
  text_imlaei: string
}




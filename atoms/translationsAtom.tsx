﻿
import { atom} from "recoil"
import { Translation } from "../typings"

export const translationDataState = atom({
    key: "translationDataState",
    default: [] 
})



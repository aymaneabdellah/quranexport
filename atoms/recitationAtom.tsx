﻿
import { atom} from "recoil"
import { VisualQuran } from "../typings"

export const currentTrackState = atom<VisualQuran | null>({
    key: "currentTrackState",
    default: null
})


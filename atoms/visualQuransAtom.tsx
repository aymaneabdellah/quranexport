﻿import { atom } from 'recoil'
import { VisualQuran } from '../typings'

export const contentListState = atom<[] | null >({
  key: 'contentListState',
  default: [],
})

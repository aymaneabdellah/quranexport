﻿import {atom} from "recoil";

export const playlistIdState = atom({
    key:"playlistIdState",
    default:"6jYqXnIKfF6EHPXnwHSJEY"
})

export const playlistState= atom ({
    key:"playlistState",
    default: null,
}) 
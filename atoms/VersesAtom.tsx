﻿import { atom } from 'recoil'
import { Translation } from '../typings'

export const VersesState = atom({
  key: 'VersesState',
  default: [],
})

export const currentVerseState = atom({
  key: 'currentVerseState',
  default: 0,
})

export const currentTranslationState = atom({
  key: 'currentTranslationState',
  default: 57,
})

export const isArabicState = atom({
  key: 'isArabicState',
  default: true,
})

export const currentChapterState = atom({
    key: 'currentChapterState',
    default: 1,
  })

